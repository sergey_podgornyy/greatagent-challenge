# Bonus points

**That tasks are not required. Only for candidates who really wants to have fun :)**

Unit testing: (hint: you may want to use function mocking / override to prevent your application for actually 
taken an action it relies on during testing, e.g. mock Twilio’s API function).  
60 % of the code you write should ideally be covered by unit tests using PHPUnit. 
Functional tests also should ideally cover all your application visitor’s activity.

Make the app a single page, mobile responsive app using a [front-end library](http://todomvc.com/)

Integrate with an API that allows you to look up a visitor’s location and automatically select the country 
closest to them. For example, if the visitor is from Sweden, automatically select Denmark whereas 
if the visitor is from Canada, select the US.

When a visitor has selected a country, the app should check if it already has a phone number in that country: 
If it has a phone number, display it to the visitor. Only if it’s the first time any visitor selects that country, 
buy the number.

Configure the number when buying it with the VoiceMethod and the VoiceURL so your app receives a request when the 
number is called

When someone calls or sends an SMS to the number, it should be add a new database record in a call log table in your 
database, and then connect the visitor’s call (hint: see “Dial” command) to your phone number in your country.

18 minutes after a vistor’s first call to the number, an SMS should be sent to the visitor thanking them for their 
call. It should be a different SMS if the call lasted more than 2 minutes as it’s more likely that you actually 
talked to the person vs. getting a voicemail.
